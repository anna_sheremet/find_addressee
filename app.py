from flask import Flask, render_template, Response
import re
import csv

app = Flask(__name__)


def split_files():
    """This function read txt with files and splits in necessary way"""
    with open('Files.txt', "r") as f:
        files = f.read().splitlines()
        for i in range(len(files)):
            files[i] = files[i].split(sep="\\")
            files[i][-1] = re.split(r"[._]", files[i][2])
    return files


def find_addressee():
    with open('Users.txt') as f:
        users = f.read().splitlines()
        for i in range(len(users)):
            users[i] = users[i].split()
    with open('Emails.txt') as f:
        emails = f.read().splitlines()
        for i in range(len(emails)):
            emails[i] = emails[i].split(sep="@")
    files = split_files()
    person_email, person_file, file_email = [], [], []
    # connect person and linked email
    for i in range(0, len(users)):
        for j in range(len(users)):
            try:
                if (emails[i][0].upper().startswith(users[j][0].upper()) or emails[i][0].upper().startswith(
                        users[j][0][0].upper())) and emails[i][0].upper().endswith(users[j][-1].upper()):
                    data = [' '.join(users[j]), '@'.join(emails[i])]
                    person_email.append(data)
            except IndexError:
                pass
    # connect person and linked file
    for i in range(0, len(users)):
        for j in range(len(users)):
            try:
                if (files[i][-1][0].upper() == users[j][0].upper() or files[i][-1][0] == users[j][0][0].upper()) and \
                        files[i][-1][-2].upper() == users[j][-1].upper():
                    with open("Files.txt", "r") as f:
                        initial_files = f.read().splitlines()
                    data = [' '.join(users[j]), initial_files[i]]
                    person_file.append(data)
            except IndexError:
                pass
    # split files again
    files = split_files()
    # connect email and linked file
    for i in range(max(len(emails), len(files))):
        for j in range(max(len(emails), len(files))):
            try:
                if emails[i][0].__contains__(files[j][-1][-2]):
                    with open("Files.txt", "r") as f:
                        initial_files = f.read().splitlines()
                        data = ['@'.join(emails[i]), initial_files[j]]
                        file_email.append(data)
            except IndexError:
                pass
    # connect all tables
    final = []
    # add notes with all three columns
    for i in range(len(file_email)):
        for j in range(len(file_email)):
            try:
                if person_email[i][1] == file_email[j][0]:
                    data = person_email[i] + [file_email[j][1]]
                    final.append(list(data))
            except IndexError:
                pass

    column_emails = [i[1] for i in final]
    # reveal note without name, check if file in file_email already is in finals
    for i in range(len(column_emails)):
        if column_emails.__contains__(file_email[i][0]):
            pass
        else:
            data = ["Null"] + [file_email[i][0]] + [file_email[i][1]]
            final.append(list(data))

    # reveal note without file, check if mail already is in files
    for i in range(len(column_emails)):
        if column_emails.__contains__(person_email[i][1]):
            pass
        else:
            data = [person_email[i][0]] + [person_email[i][1]] + ["Null"]
            final.append(list(data))

    # reveal note without email, check if file already is in final
    column_files = [i[2] for i in final]
    for i in range(len(column_files)):
        if column_files.__contains__(person_file[i][1]):
            pass
        else:
            data = [person_file[i][0]] + ["Null"] + [person_file[i][1]]
            final.append(list(data))

    # reveal note with only email
    column_emails = [i[1] for i in final]
    for i in range(len(emails)):
        if column_emails.__contains__('@'.join(emails[i])):
            pass
        else:
            data = ["Null"] + ['@'.join(emails[i])] + ["Null"]
            final.append(list(data))

    # reveal note with only file
    column_files = [i[2] for i in final]
    for i in range(len(files)):
        if column_files.__contains__(files[i]):
            data = ["Null"] + ["Null"] + [files[i]]
            final.append(list(data))

    # reveal note with only name
    column_names = [i[0] for i in final]
    for i in range(len(users)):
        if column_names.__contains__(users[i]):
            data = [users[i]] + ["Null"] + ["Null"]

    # create/open csv file and make it able to write list
    with open("output.csv", "w", newline="") as f:
        writer = csv.writer(f)
        writer.writerows(final)

    # read csv to download it in browser
    with open("output.csv", "r") as f:
        final_csv = f.read()

    # output of function
    return final_csv


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/csv/')
def csv_return():
    final_csv = find_addressee()
    return Response(
        final_csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                     "attachment; filename='output.csv'"})


if __name__ == '__main__':
    app.run()
